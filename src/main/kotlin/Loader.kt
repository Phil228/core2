package main

import org.bukkit.Bukkit
import org.bukkit.command.CommandExecutor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

class Loader : JavaPlugin(), Listener, CommandExecutor {
    override fun onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this)
    }


    @EventHandler
    public fun joinEvent(event: PlayerJoinEvent) {
        event.player.sendMessage("§8[§6Core§8] §7Welcome back")
        Bukkit.broadcastMessage("§8[§6Core§8]§b " + event.player.name + " §7hat den Server betreten!")
    }
}